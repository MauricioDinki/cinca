if (document.querySelector("#menuContainer")) {
  function showMenu() {
    document.querySelector("#menuContainer").style.width = "100%";
    document.querySelector("#menuContainer").style.background =
      "rgba(0,0,0,0.75)";
  }

  function hideMenu() {
    document.querySelector("#menuContainer").style.width = "0%";
    document.querySelector("#menuContainer").style.background = "white";
  }

  /* Events of touchstart to mobile version*/

  let showHamburger = document.querySelector("#hamburgerMenu");

  showHamburger.addEventListener("touchstart", function (e) {
    e.preventDefault();
    showMenu();
  });

  let hideHamburger = document.querySelector("#hideHamburger");

  hideHamburger.addEventListener("click", function (e) {
    e.preventDefault();
    hideMenu();
  });

  /* Events of click to desktop version*/

  showHamburger.addEventListener("click", function (e) {
    e.preventDefault();
    showMenu();
  });

  hideHamburger.addEventListener("click", function (e) {
    e.preventDefault();
    hideMenu();
  });
}
