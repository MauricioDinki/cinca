import Vue from "vue";
import ProvidersComponent from "./components/ProvidersComponent.vue";
if (document.querySelector("#salePage")) {
  const app = new Vue({
    el: "#salePage",
    components: {
      "providers-component": ProvidersComponent,
    },
    data() {
      return {};
    },
  });
}

if (document.querySelector("#contactForm")) {
  function ajax(method, url, data, success, error) {
    let xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = function () {
      if (xhr.readyState !== XMLHttpRequest.DONE) return;
      if (xhr.status === 200) {
        success(xhr.response, xhr.responseType);
      } else {
        error(xhr.status, xhr.response, xhr.responseType);
      }
    };
    xhr.send(data);
  }
  let form = document.getElementById("contactForm");
  let button = document.getElementById("contactFormBtn");
  let status = document.getElementById("contactFormStatus");
  // Success and Error functions for after the form is submitted
  function success() {
    form.reset();
    button.style = "display: none ";
    status.style = "display: block ";
    status.innerHTML = "Mensaje Enviado";
  }
  function error() {
    status.innerHTML = "Oops! Ocurrio un problema.";
  }
  // handle the form submission event
  form.addEventListener("submit", function (ev) {
    ev.preventDefault();
    let response = grecaptcha.getResponse();
    // eslint-disable-next-line eqeqeq
    if (response.length == 0) {
      //reCaptcha not verified
      alert("Verifica que eres humano");
      return false;
    }
    let data = new FormData(form);
    for (let value of data.values()) {
      console.log(value);
    }
    ajax(form.method, form.action, data, success, error);
  });
}
