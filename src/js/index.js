import "../styl/main.styl";
import "./menu";

require.context("../img/", true, /\.(png|svg|jpg|gif|webp|jpeg|ico)$/);
require.context("../pdfs/", true, /\.(pdf)$/);

import "./controllers";
